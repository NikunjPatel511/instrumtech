
<!DOCTYPE html>
<html lang="zxx">
<?php include('include/headerscript.php'); ?>

<body>

<?php include('include/header.php'); ?>
 
	<!-- BANNER -->
	<div class="section banner-page about">
		<div class="container">
			<div class="row">
				<div class="col-sm-12 col-md-12">
					<div class="title-page">About Us</div>
					<ol class="breadcrumb">
						<li><a href="index.php">Home</a></li>
						<li class="active">About Us</li>
					</ol>
				</div>
			</div>
		</div>
	</div>
	 
	<!-- ABOUT FEATURE -->
	<div class="section feature">
		<div class="container">

			<div class="row">
				<div class="col-sm-12 col-md-12">
					<h2 class="section-heading">
						About Us
					</h2>
				</div>
			</div>

			<div class="row">
				
				<div class="col-sm-6 col-md-6">
					<div id="caro" class="owl-carousel owl-theme">
						<div class="item">
							<img src="images/blog-2.jpg" alt="Company History">
						</div>
						<div class="item">
							<img src="images/blog-3.jpg" alt="Company History">
						</div>
						<div class="item">
							<img src="images/blog-4.jpg" alt="Company History">
						</div>

					</div>
				</div>
				<div class="col-sm-6 col-md-6">
					<h3>INSTRUMTECH SOLUTIONS</h3>
					<p>“INSTRUMTECH” was established in early 2015 for providing solutions in the field of Instrumentation, Automation & Electrical. We are one of the fastest growing suppliers & manufacturer of Process Instruments, Custom Hydraulic/ Pneumatic Test Benches, PLC Based Automation Systems, Control Panel design & development as well as Electrical Panels. Our customers are Multi specialty Hospitals, large national / multinational companies in the field of chemical, pharmaceutical, Oil & Gas Industries etc.</p> 
					<p><em>“INSTRUMTECH” also deals in Calibration/Validating services, MS/SS/Copper Tubing & Piping, Software development, Wire laying, lugging & tagging and also provides On-Site Support for repair & diagnose control panel malfunctions, instrumentation malfunction etc.</em></p> 
				</div>

				<div class="clearfix"></div>
			</div>
		</div>
	</div>

	<!-- MISSION -->
	<div class="section section-border">
		<div class="container">
			<div class="row">
				<div class="col-sm-12 col-md-12">
					<h2 class="section-heading">
						WHY CHOOSING US
					</h2>
				</div>
			</div>

			<div class="row">
				<div class="col-sm-4 col-md-4">
					<!-- BOX 1 -->
					<div class="box-image-2 box_card margin-bottom-30">
						<div class="image">
							<img src="images/blog-3.jpg" alt="">
						</div>
						<div class="blok-title">VISION / GOAL</div> 
						<div class="description">“INSTRUMTECH” was established with the goal of providing “HIGH END – ONE PLACE” Solutions in the field of Instrumentation, Automation & Electrical. Furthermore we believe to supply, design and develop adequate quality products for the application with most economic prizes. Customers and Employees satisfaction is our prime motto.</div>
					</div>
				</div>
				<div class="col-sm-4 col-md-4">
					<!-- BOX 2 -->
					<div class="box-image-2 box_card margin-bottom-30">
						<div class="image">
							<img src="images/blog-4.jpg" alt="">
						</div>
						<div class="blok-title">SALES &AMP; MARKETING</div> 
						<div class="description">“INSTRUMTECH” has qualified & trained sales & marketing division who enormously works on studying customer needs and building relations with customers and always in seek of customer feedback to improve products & services. This team is experienced in understanding customer requirements, suggesting adequate product and demonstrating the product.</div>
					</div>
				</div>
				<div class="col-sm-4 col-md-4">
					<!-- BOX 1 -->
					<div class="box-image-2 box_card margin-bottom-30">
						<div class="image">
							<img src="images/blog-2.jpg" alt="">
						</div>
						<div class="blok-title">SERVICE &AMP; SUPPORT</div> 
						<div class="description">“INSTRUMETECH” has dedicated team who provides after sales service and support to the customers. These technical professionals are always ready to provide assistance in installation, irrection & commissioning to the customers and also provides onsite support If required.</div>
					</div>
				</div>
				
			</div>
		</div>
	</div>
		 
	
	 
	<?php include('include/footer.php'); ?>
	<?php include('include/footerscript.php'); ?>
		
</body>
</html>