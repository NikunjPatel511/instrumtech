
<!DOCTYPE html>
<html lang="zxx">
<?php include('include/headerscript.php'); ?>
<link rel="stylesheet" type="text/css" href="css/vendor/css/vendor/jquery.fancybox.css">
<link rel="stylesheet" type="text/css" href="css/vendor/jquery.fancybox.min.css">

<body>

<?php include('include/header.php'); ?>
 
	<!-- BANNER -->
	<div class="section banner-page about">
		<div class="container">
			<div class="row">
				<div class="col-sm-12 col-md-12">
					<div class="title-page">Photo Gallery</div>
					<ol class="breadcrumb">
						<li><a href="index.php">Home</a></li>
						<li class="active">Photo Gallery</li>
					</ol>
				</div>
			</div>
		</div>
	</div>

	<!-- Team -->
	<div class="section why ">
		<div class="container">
			<div class="row grid-services">
				<div class="col-sm-6 col-md-4">
					<div class="box_card_product">
						<a  href="images/project-img-2.jpg" data-fancybox="images">
							<div class="media">
								<img src="images/project-img-2.jpg" alt="" class="img-responsive">
							</div>
							<div class="body">
								<div class="content">
									<h4 class="title">The Gas Company</h4>
									<span class="category">Commodoenim</span>
								</div>
							</div>
						</a>
					</div>
				</div>
				<div class="col-sm-6 col-md-4">
					<div class="box_card_product">
						<a  href="images/project-img-6.jpg" data-fancybox="images">
							<div class="media">
								<img src="images/project-img-6.jpg" alt="" class="img-responsive">
							</div>
							<div class="body">
								<div class="content">
									<h4 class="title">The Gas Company</h4>
									<span class="category">Commodoenim</span>
								</div>
							</div>
						</a>
					</div>
				</div>
				<div class="col-sm-6 col-md-4">
					<div class="box_card_product">
						<a  href="images/project-img-5.jpg" data-fancybox="images">
							<div class="media">
								<img src="images/project-img-5.jpg" alt="" class="img-responsive">
							</div>
							<div class="body">
								<div class="content">
									<h4 class="title">The Gas Company</h4>
									<span class="category">Commodoenim</span>
								</div>
							</div>
						</a>
					</div>
				</div>
				<div class="col-sm-6 col-md-4">
					<div class="box_card_product">
						<a  href="images/project-img-4.jpg" data-fancybox="images">
							<div class="media">
								<img src="images/project-img-4.jpg" alt="" class="img-responsive">
							</div>
							<div class="body">
								<div class="content">
									<h4 class="title">The Gas Company</h4>
									<span class="category">Commodoenim</span>
								</div>
							</div>
						</a>
					</div>
				</div>
				<div class="col-sm-6 col-md-4">
					<div class="box_card_product">
						<a  href="images/project-img-3.jpg" data-fancybox="images">
							<div class="media">
								<img src="images/project-img-3.jpg" alt="" class="img-responsive">
							</div>
							<div class="body">
								<div class="content">
									<h4 class="title">The Gas Company</h4>
									<span class="category">Commodoenim</span>
								</div>
							</div>
						</a>
					</div>
				</div>
				<div class="col-sm-6 col-md-4">
					<div class="box_card_product">
						<a  href="images/project-img-2.jpg" data-fancybox="images">
							<div class="media">
								<img src="images/project-img-2.jpg" alt="" class="img-responsive">
							</div>
							<div class="body">
								<div class="content">
									<h4 class="title">The Gas Company</h4>
									<span class="category">Commodoenim</span>
								</div>
							</div>
						</a>
					</div>
				</div>
				


			</div>
		</div>
	</div>	


	 
	
	
	 
	<?php include('include/footer.php'); ?>
	<?php include('include/footerscript.php'); ?>
	<script src="js/vendor/jquery.fancybox.js"></script>
	<script type="text/javascript">
		$(document).ready(function() {

			
		$('[data-fancybox]').fancybox({
			  protect: true,
			  buttons : [
			    'zoom',
			    'thumbs',
			    'close'
			  ]
			});
		});

	</script>

		
</body>
</html>