
<!DOCTYPE html>
<html lang="zxx">
<?php include('include/headerscript.php'); ?>

<body>

<?php include('include/header.php'); ?>
 
	<!-- BANNER -->
	<div class="section banner-page about">
		<div class="container">
			<div class="row">
				<div class="col-sm-12 col-md-12">
					<div class="title-page">Products</div>
					<ol class="breadcrumb">
						<li><a href="index.php">Products</a></li>
						<li class="active">Products</li>
					</ol>
				</div>
			</div>
		</div>
	</div>
	
	
	<!-- ABOUT FEATURE -->
	<div class="section why">
		<div class="container">
			<div class="row box_card_product">
				<div class="col-sm-6 col-md-6">

					<h3>Process Instrument</h3>
					<p>“INSTRUMTECH” is one of the fastest growing supplier & manufacturer of the process instruments. Our high-end products are perfect combination of excellent visual aesthetics, quality assurance and durable technology.</p> 
					<ul class="bull">
						<li>Loop Power Indicators</li>
						<li>Universal Multi Display Indicator</li>
						<li>Provide repair services to a diverse customer base across multiple sectors</li>
						<li>Remain responsive to our customers’ needs</li>
						<li>Work fewer hours — and make more money</li>
						<li>Attract and retain quality, high-paying customers</li>
						<li>Manage your time so you’ll get more done in less time</li>
						
					</ul>
					
				</div>
				<div class="col-sm-6 col-md-6">
					<div class="vidimg">
						<img src="images/service-detail-1.jpg" alt="" class="img-responsive">
					</div>
				</div>
				
			</div>

			<div class="row box_card_product">
				<div class="col-sm-6 col-md-6">

					<h3>Custom Instrumentation Equipment</h3>
					<p>“INSTRUMTECH” is one of the fastest growing supplier & manufacturer of the process instruments. Our high-end products are perfect combination of excellent visual aesthetics, quality assurance and durable technology.</p> 
					<ul class="bull">
						<li>Loop Power Indicators</li>
						<li>Universal Multi Display Indicator</li>
						<li>Provide repair services to a diverse customer base across multiple sectors</li>
						<li>Remain responsive to our customers’ needs</li>
						<li>Work fewer hours — and make more money</li>
						<li>Attract and retain quality, high-paying customers</li>
						<li>Manage your time so you’ll get more done in less time</li>
						
					</ul>
					
				</div>
				<div class="col-sm-6 col-md-6">
					<div class="vidimg">
						<img src="images/service-detail-1.jpg" alt="" class="img-responsive">
					</div>
				</div>
				
			</div>

			<div class="row box_card_product">
				<div class="col-sm-6 col-md-6">

					<h3>Control Panel</h3>
					<p>“INSTRUMTECH” is one of the fastest growing supplier & manufacturer of the process instruments. Our high-end products are perfect combination of excellent visual aesthetics, quality assurance and durable technology.</p> 
					<ul class="bull">
						<li>Loop Power Indicators</li>
						<li>Universal Multi Display Indicator</li>
						<li>Provide repair services to a diverse customer base across multiple sectors</li>
						<li>Remain responsive to our customers’ needs</li>
						<li>Work fewer hours — and make more money</li>
						<li>Attract and retain quality, high-paying customers</li>
						<li>Manage your time so you’ll get more done in less time</li>
						
					</ul>
					
				</div>
				<div class="col-sm-6 col-md-6">
					<div class="vidimg">
						<img src="images/service-detail-1.jpg" alt="" class="img-responsive">
					</div>
				</div>
				
			</div>
			<div class="row box_card_product">
				<div class="col-sm-6 col-md-6">

					<h3>Software Development</h3>
					<p>“INSTRUMTECH” is one of the fastest growing supplier & manufacturer of the process instruments. Our high-end products are perfect combination of excellent visual aesthetics, quality assurance and durable technology.</p> 
					<ul class="bull">
						<li>Loop Power Indicators</li>
						<li>Universal Multi Display Indicator</li>
						<li>Provide repair services to a diverse customer base across multiple sectors</li>
						<li>Remain responsive to our customers’ needs</li>
						<li>Work fewer hours — and make more money</li>
						<li>Attract and retain quality, high-paying customers</li>
						<li>Manage your time so you’ll get more done in less time</li>
						
					</ul>
					
				</div>
				<div class="col-sm-6 col-md-6">
					<div class="vidimg">
						<img src="images/service-detail-1.jpg" alt="" class="img-responsive">
					</div>
				</div>
				
			</div>
		</div>
	</div>

	 
	<?php include('include/footer.php'); ?>
	<?php include('include/footerscript.php'); ?>
		
</body>
</html>