
<!DOCTYPE html>
<html lang="zxx">
<?php include('include/headerscript.php'); ?>
<link rel="stylesheet" type="text/css" href="css/vendor/css/vendor/jquery.fancybox.css">
<link rel="stylesheet" type="text/css" href="css/vendor/jquery.fancybox.min.css">

<body>

<?php include('include/header.php'); ?>
 
	<!-- BANNER -->
	<div class="section banner-page about">
		<div class="container">
			<div class="row">
				<div class="col-sm-12 col-md-12">
					<div class="title-page">Video Gallery</div>
					<ol class="breadcrumb">
						<li><a href="index.php">Home</a></li>
						<li class="active">Video Gallery</li>
					</ol>
				</div>
			</div>
		</div>
	</div>

	<!-- Team -->
	<div class="section why ">
		<div class="container">
			<div class="row grid-services">
				<div class="col-sm-6 col-md-4">
					<div class="box_card_product">
						<iframe width="335" height="250" src="https://www.youtube.com/embed/uBEqCCoVkSI" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
					</div>
				</div>
				<div class="col-sm-6 col-md-4">
					<div class="box_card_product">
						<iframe width="335" height="250" src="https://www.youtube.com/embed/uBEqCCoVkSI" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
					</div>
				</div>
				<div class="col-sm-6 col-md-4">
					<div class="box_card_product">
						<iframe width="335" height="250" src="https://www.youtube.com/embed/uBEqCCoVkSI" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
					</div>
				</div>
				<div class="col-sm-6 col-md-4">
					<div class="box_card_product">
						<iframe width="335" height="250" src="https://www.youtube.com/embed/uBEqCCoVkSI" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
					</div>
				</div>
				<div class="col-sm-6 col-md-4">
					<div class="box_card_product">
						<iframe width="335" height="250" src="https://www.youtube.com/embed/uBEqCCoVkSI" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
					</div>
				</div>
				<div class="col-sm-6 col-md-4">
					<div class="box_card_product">
						<iframe width="335" height="250" src="https://www.youtube.com/embed/uBEqCCoVkSI" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
					</div>
				</div>
				
				


			</div>
		</div>
	</div>	


	 
	
	
	 
	<?php include('include/footer.php'); ?>
	<?php include('include/footerscript.php'); ?>
	<script src="js/vendor/jquery.fancybox.js"></script>
	<script type="text/javascript">
		$(document).ready(function() {

			
		$('[data-fancybox]').fancybox({
			  protect: true,
			  buttons : [
			    'zoom',
			    'thumbs',
			    'close'
			  ]
			});
		});

	</script>

		
</body>
</html>