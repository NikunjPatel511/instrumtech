
<!DOCTYPE html>
<html lang="zxx">
<?php include('include/headerscript.php'); ?>

<body>

<?php include('include/header.php'); ?>
 
	<!-- BANNER -->
	<div class="section banner-page about">
		<div class="container">
			<div class="row">
				<div class="col-sm-12 col-md-12">
					<div class="title-page">About Us</div>
					<ol class="breadcrumb">
						<li><a href="index.php">About Us</a></li>
						<li class="active">About Us</li>
					</ol>
				</div>
			</div>
		</div>
	</div>
	 
	<!-- ABOUT FEATURE -->
	<div class="section feature overlap">
		<div class="container">

			<div class="row">
				
				<div class="col-sm-4 col-md-4">
					<!-- BOX 1 -->
					<div class="box-icon-2">
						<div class="icon">
							<div class="fa fa-star-o"></div>
						</div>
						<div class="body-content">
							<div class="heading">QUALITY DRIVEN</div>
							Lorem ipsum dolor sit amet, consectetur adipisicing elit sed do eiusmod tempor.
						</div>
					</div>
				</div>
				<div class="col-sm-4 col-md-4">
					<!-- BOX 2 -->
					<div class="box-icon-2">
						<div class="icon">
							<div class="fa fa-umbrella"></div>
						</div>
						<div class="body-content">
							<div class="heading">CUSTOMER FOCUSED</div>
							Lorem ipsum dolor sit amet, consectetur adipisicing elit sed do eiusmod tempor.
						</div>
					</div>
				</div>
				<div class="col-sm-4 col-md-4">
					<!-- BOX 3 -->
					<div class="box-icon-2">
						<div class="icon">
							<div class="fa fa-users"></div>
						</div>
						<div class="body-content">
							<div class="heading">GLOBAL SOURCING</div>
							Lorem ipsum dolor sit amet, consectetur adipisicing elit sed do eiusmod tempor.
						</div>
					</div>
				</div>
				
			</div>

			<div class="row">
				<div class="col-sm-12 col-md-12">
					<h2 class="section-heading">
						COMPANY OVERVIEW
					</h2>
				</div>
			</div>

			<div class="row">
				<div class="col-sm-5 col-md-5">
					<div class="jumbo-heading">
						<h2>OUR REAL COMMITMENT REACHES BEYOND GAS &amp; OIL COMPANY.</h2>
						<span class="fa fa-paper-plane-o"></span>
					</div>
				</div>
				<div class="col-sm-7 col-md-7">
					<p class="lead">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam.</p> 
					<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p> 
				</div>

			</div>

		</div>
	</div>
	 
	<!-- WHY -->
	<div class="section why section-border bglight">
		<div class="container">
			<div class="row">
				
				<div class="col-sm-5 col-md-5">
					<div class="director-image">
						<div class="director-image-title">HEAD OF OPERATIONS</div>
						<img src="images/director.png" alt="bos-photo">
					</div>
					<div class="margin-bottom-30"></div>
				</div>
				<div class="col-sm-7 col-md-7">
					<h3 class="director-title">Peter White</h3>
					<div class="director-position">HEAD OF OPERATIONS</div>
					<div class="margin-bottom-30"></div>
					<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
					<p>Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
					<p>&nbsp;</p>
						
					<blockquote>
						<p>Petro Industrial Template continues to grow ever day thanks to the confidence our clients have in us. We cover many industries such as oil gas, energy, business services, consumer products.</p>
					</blockquote>
				</div>
				
			</div>
		</div>
	</div>

	<!-- Statistic -->
	<div class="section statistic bgsection">
		<div class="container">
			<div class="row">
				
				<div class="col-sm-3 col-md-3">
					<div class="counter-1">
			            <div class="counter-number">
			              10
			            </div>
			            <div class="counter-title">Offices<br> Worldwide </div>
		          	</div>
				</div>
				<div class="col-sm-3 col-md-3">
					<div class="counter-1">
			            <div class="counter-number">
			              10
			            </div>
			            <div class="counter-title">Refineries &amp; <br>Operations</div>
		          	</div>
				</div>
				<div class="col-sm-3 col-md-3">
					<div class="counter-1">
			            <div class="counter-number">
			              5700
			            </div>
			            <div class="counter-title">Satisfied<br>Employees</div>
		          	</div>
				</div>
				<div class="col-sm-3 col-md-3">
					<div class="counter-1">
			            <div class="counter-number">
			              22
			            </div>
			            <div class="counter-title">Awards &amp; <br>Recognitions</div>
		          	</div>
				</div>

			</div>
		</div>
	</div> 

	<!-- WHY -->
	<div class="section why">
		<div class="container">
			<div class="row">
				<div class="col-sm-12 col-md-12">
					
				</div>
				<div class="col-sm-6 col-md-6">

					<h3>Our Mission</h3>
					<p>Renim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p> 
					<ul class="bull">
						<li>Offer a broad range of cost-effective industrial solutions</li>
						<li>Maintain a robust inventory of parts and products</li>
						<li>Provide repair services to a diverse customer base across multiple sectors</li>
						<li>Remain responsive to our customers’ needs</li>
						<li>Work fewer hours — and make more money</li>
						<li>Attract and retain quality, high-paying customers</li>
						<li>Manage your time so you’ll get more done in less time</li>
						
					</ul>
					
				</div>
				<div class="col-sm-6 col-md-6">
					<div class="vidimg">
						<div class="play-vid">
							<a class="popup-youtube" href="https://www.youtube.com/watch?v=JGYuCRYFxew"><span class="fa fa-play fa-3x playvid"></span></a>
						</div>
						<img src="images/welcome-img.jpg" alt="" class="img-responsive">
					</div>
				</div>
				
			</div>
		</div>
	</div>
	 
	<!-- MISSION -->
	<div class="section section-border">
		<div class="container">
			<div class="row">
				<div class="col-sm-12 col-md-12">
					<h2 class="section-heading">
						WHY CHOOSING US
					</h2>
				</div>
			</div>

			<div class="row">
				<div class="col-sm-4 col-md-4">
					<!-- BOX 1 -->
					<div class="box-image-2 margin-bottom-30">
						<div class="image">
							<img src="images/blog-3.jpg" alt="">
						</div>
						<div class="blok-title">We Are Professional</div> 
						<div class="description">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</div>
					</div>
				</div>
				<div class="col-sm-4 col-md-4">
					<!-- BOX 2 -->
					<div class="box-image-2 margin-bottom-30">
						<div class="image">
							<img src="images/blog-4.jpg" alt="">
						</div>
						<div class="blok-title">We Are Trusted</div> 
						<div class="description">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</div>
					</div>
				</div>
				<div class="col-sm-4 col-md-4">
					<!-- BOX 1 -->
					<div class="box-image-2 margin-bottom-30">
						<div class="image">
							<img src="images/blog-2.jpg" alt="">
						</div>
						<div class="blok-title">We Are Expert</div> 
						<div class="description">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</div>
					</div>
				</div>
				
			</div>
		</div>
	</div>
		 
	
	<!-- INFO BOX -->
	<div class="section info overlap-bottom">
		<div class="container">
			<div class="row">
				
				<div class="col-sm-4 col-md-4">
					<!-- BOX 1 -->
					<div class="box-icon-4">
						<div class="icon"><i class="fa fa-phone"></i></div>
						<div class="body-content">
							<div class="heading">CALL US NOW</div>
							Office Telephone: +62 800 9000 123 <br>
							Mobile: +62 800 9000 123
						</div>
					</div>
				</div>
				<div class="col-sm-4 col-md-4">
					<!-- BOX 2 -->
					<div class="box-icon-4">
						<div class="icon"><i class="fa fa-map-marker"></i></div>
						<div class="body-content">
							<div class="heading">COME VISIT US</div>
							99 S.t Jomblo Park Pekanbaru 28292. Indonesia
						</div>
					</div>
				</div>
				<div class="col-sm-4 col-md-4">
					<!-- BOX 3 -->
					<div class="box-icon-4">
						<div class="icon"><i class="fa fa-envelope"></i></div>
						<div class="body-content">
							<div class="heading">SEND US A MESSAGE</div>
							General: info@petro.com<br>
							Sales: sales@petro.com
						</div>
					</div>
				</div>
				
			</div>

		</div>
	</div>
	 
	<?php include('include/footer.php'); ?>
	<?php include('include/footerscript.php'); ?>
		
</body>
</html>