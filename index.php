<!DOCTYPE html>
<html lang="zxx">
<?php include('include/headerscript.php'); ?>
<body>
<!-- Load page -->
   <div class="animationload">
      <div class="loader"></div>
   </div>
   
   <!-- BACK TO TOP SECTION -->
   <a href="#0" class="cd-top cd-is-visible cd-fade-out">Top</a>
   
	<?php include('include/header.php'); ?>
	<!-- BANNER -->
	<div id="slides" class="section banner">
		<ul class="slides-container">
			<li>
				<img src="images/main-slider/slider1.jpg" alt="">
				<div class="overlay-bg"></div>
				<div class="container">
					<div class="wrap-caption">
						<h2 class="caption-heading">
							Make Your Best Future
						</h2>
						<p class="excerpt">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod</p>	
						<a href="#" class="btn btn-primary" title="LEARN MORE">LEARN MORE</a> <a href="#" class="btn btn-secondary" title="CONTACT US">CONTACT US</a>
					</div>
				</div>
			</li>
			<li>
				<img src="images/main-slider/slider2.jpg" alt="">
				<div class="overlay-bg"></div>
				
			</li>
			<li>
				<img src="images/main-slider/slider7.jpg" alt="">
				<div class="overlay-bg"></div>
				
			</li>
			<li>
				<img src="images/main-slider/slider6.jpg" alt="">
				<div class="overlay-bg"></div>
				
			</li><li>
				<img src="images/main-slider/slider5.jpg" alt="">
				<div class="overlay-bg"></div>
			
			</li>
			
		</ul>

		<nav class="slides-navigation">
			<div class="container">
				<a href="#" class="next">
					<i class="fa fa-chevron-right"></i>
				</a>
				<a href="#" class="prev">
					<i class="fa fa-chevron-left"></i>
				</a>
	      	</div>
	    </nav>
		
	</div>

	<!-- ABOUT FEATURE -->
	<div class="section feature overlap">
		<div class="container">

			<div class="row">
				
				<div class="col-sm-4 col-md-4">
					<!-- BOX 1 -->
					<div class="box-icon-2">
						<div class="icon">
							<div class="fa fa-star-o"></div>
						</div>
						<div class="body-content">
							<div class="heading">QUALITY DRIVEN</div>
							Lorem ipsum dolor sit amet, consectetur adipisicing elit sed do eiusmod tempor.
						</div>
					</div>
				</div>
				<div class="col-sm-4 col-md-4">
					<!-- BOX 2 -->
					<div class="box-icon-2">
						<div class="icon">
							<div class="fa fa-umbrella"></div>
						</div>
						<div class="body-content">
							<div class="heading">CUSTOMER FOCUSED</div>
							Lorem ipsum dolor sit amet, consectetur adipisicing elit sed do eiusmod tempor.
						</div>
					</div>
				</div>
				<div class="col-sm-4 col-md-4">
					<!-- BOX 3 -->
					<div class="box-icon-2">
						<div class="icon">
							<div class="fa fa-users"></div>
						</div>
						<div class="body-content">
							<div class="heading">GLOBAL SOURCING</div>
							Lorem ipsum dolor sit amet, consectetur adipisicing elit sed do eiusmod tempor.
						</div>
					</div>
				</div>
				
			</div>



			<div class="row">
				<div class="col-sm-12 col-md-12">
					<h2 class="section-heading">
						Excellent Solution For Your Business
					</h2>
				</div>
			</div>

			

			<div class="row">
				<div class="col-md-5 col-sm-5">
					<img src="images/main-slider/2.gif">
				</div>
				
				<div class="col-sm-7 col-md-7">
					<p class="lead">"INSTRUMTECH" was established in early 2015 for providing solutions in the field of Instrumentation, Automation & Electrical. We are one of the fastest growing suppliers & manufacturer of Process Instruments, Custom Hydraulic/ Pneumatic Test Benches, PLC Based Automation Systems, Control Panel design & development as well as Electrical Panels. Our customers are Multi specialty Hospitals, large national / multinational companies in the field of chemical, pharmaceutical, Oil & Gas Industries etc.</p> 
					<p class="lead">"INSTRUMTECH" also deals in Calibration/Validating services, MS/SS/Copper Tubing & Piping, Software development, Wire laying, lugging & tagging and also provides On-Site Support for repair & diagnose control panel malfunctions, instrumentation malfunction etc.</p> 
					
				</div>

			</div>

		</div>
	</div>

	<!-- CTA -->
	<div class="section cta">
		<div class="container">
			
			<div class="row">
				<div class="col-sm-12 col-md-12">
					<div class="cta-info">
						<h3>If you need industrial solution... We are available for you</h3>
						<p>We provide innovative solutions for sustainable progress. Our professional team works to increase productivity and cost effectiveness on the market</p>
						<a href="contact-2.html" title="" class="btn btn-cta">CONTACT US</a>
					</div>
				</div>
			</div>
		</div>
	</div>
	 
	<!-- SERVICES -->
	<div class="section service section-border bglight">
		<div class="container">
			<div class="row">
				<div class="col-sm-12 col-md-12">
					<h2 class="section-heading">
						SERVICES
					</h2>
				</div>
			</div>

			<div class="row">

				<div class="col-sm-6 col-md-4">
					<!-- BOX 1 -->
					<div class="feature-box-8">
		              <div class="media">
		                <img src="images/service-1.jpg" alt="rud" class="img-responsive">
		              </div>
		              <div class="body">
		                <div class="icon-holder">
		                  <span class="fa fa-gears"></span>
		                </div>
		                <a href="services-detail.html" class="title">MECHANICAL ENGINEERING</a>
		                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit sed</p>
		                <a href="services-detail.html" class="readmore">READ MORE</a>
		              </div>
		            </div>
				</div>
				<div class="col-sm-6 col-md-4">
					<!-- BOX 2 -->
					<div class="feature-box-8">
		              <div class="media">
		                <img src="images/service-2.jpg" alt="rud" class="img-responsive">
		              </div>
		              <div class="body">
		                <div class="icon-holder">
		                  <span class="fa fa-leaf"></span>
		                </div>
		                <a href="services-detail.html" class="title">AGRICULTURAL PROCESSING</a>
		                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit sed</p>
		                <a href="services-detail.html" class="readmore">READ MORE</a>
		              </div>
		            </div>
				</div>
				<div class="col-sm-6 col-md-4">
					<!-- BOX 3 -->
					<div class="feature-box-8">
		              <div class="media">
		                <img src="images/service-3.jpg" alt="rud" class="img-responsive">
		              </div>
		              <div class="body">
		                <div class="icon-holder">
		                  <span class="fa fa-fire"></span>
		                </div>
		                <a href="services-detail.html" class="title">OILS AND LUBRICANTS</a>
		                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit sed</p>
		                <a href="services-detail.html" class="readmore">READ MORE</a>
		              </div>
		            </div>
				</div>
				<div class="col-sm-6 col-md-4">
					<!-- BOX 4 -->
					<div class="feature-box-8">
		              <div class="media">
		                <img src="images/service-4.jpg" alt="rud" class="img-responsive">
		              </div>
		              <div class="body">
		                <div class="icon-holder">
		                  <span class="fa fa-flash"></span>
		                </div>
		                <a href="services-detail.html" class="title">POWER AND ENERGY</a>
		                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit sed</p>
		                <a href="services-detail.html" class="readmore">READ MORE</a>
		              </div>
		            </div>
	          	</div>
				<div class="col-sm-6 col-md-4">
					<!-- BOX 5 -->
					<div class="feature-box-8">
		              <div class="media">
		                <img src="images/service-5.jpg" alt="rud" class="img-responsive">
		              </div>
		              <div class="body">
		                <div class="icon-holder">
		                  <span class="fa fa-flask"></span>
		                </div>
		                <a href="services-detail.html" class="title">CHEMICAL RESEARCH</a>
		                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit sed</p>
		                <a href="services-detail.html" class="readmore">READ MORE</a>
		              </div>
		            </div>
				</div>
				<div class="col-sm-6 col-md-4">
					<!-- BOX 6 -->
					<div class="feature-box-8">
		              <div class="media">
		                <img src="images/service-6.jpg" alt="rud" class="img-responsive">
		              </div>
		              <div class="body">
		                <div class="icon-holder">
		                  <span class="fa fa-cubes"></span>
		                </div>
		                <a href="services-detail.html" class="title">MATERIAL ENGINEERING</a>
		                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit sed</p>
		                <a href="services-detail.html" class="readmore">READ MORE</a>
		              </div>
		            </div>
				</div>			
				
			</div>
		</div>
	</div>
	 
	

	<!-- PROJECTS -->
	<div class="section">
		<div class="container">
			<div class="row">
				<div class="col-sm-12 col-md-12">
					<h2 class="section-heading">
						PROJECTS
					</h2>
				</div>
			</div>
			<div class="row">
				<div class="col-sm-12 col-md-12">
					<nav class="categories">
					<ul class="portfolio_filter dark">
						<li><a href="#" class="active" data-filter="*">all</a></li>
						<li><a href="#" data-filter=".eco">Eco</a></li>
						<li><a href="#" data-filter=".manufacturing">Manufacturing</a></li>
						<li><a href="#" data-filter=".industry">Industry</a></li>
						<li><a href="#" data-filter=".oil">Oil</a></li>
						<li><a href="#" data-filter=".gas">Gas</a></li>
						<li><a href="#" data-filter=".factory">Factory</a></li>
					</ul>
				</nav>
				</div>
			</div>
			<div class="row grid-services">
				<div class="col-sm-6 col-md-4 eco manufacturing gas">
					<div class="box-image-4">
						<a href="project-detail.html" title="Industrial Complex">
							<div class="media">
								<img src="images/project-img-1.jpg" alt="" class="img-responsive">
							</div>
							<div class="body">
								<div class="content">
									<h4 class="title">INDUSTRIAL COMPLEX</h4>
									<span class="category">Commodoenim</span>
								</div>
							</div>
						</a>
					</div>
				</div>
				<div class="col-sm-6 col-md-4 manufacturing gas">
					<div class="box-image-4">
						<a href="project-detail.html" title="Industrial Complex">
							<div class="media">
								<img src="images/project-img-2.jpg" alt="" class="img-responsive">
							</div>
							<div class="body">
								<div class="content">
									<h4 class="title">The Gas Company</h4>
									<span class="category">Commodoenim</span>
								</div>
							</div>
						</a>
					</div>
				</div>
				<div class="col-sm-6 col-md-4 industry factory">
					<div class="box-image-4">
						<a href="project-detail.html" title="Industrial Complex">
							<div class="media">
								<img src="images/project-img-3.jpg" alt="" class="img-responsive">
							</div>
							<div class="body">
								<div class="content">
									<h4 class="title">Warehouse Industry</h4>
									<span class="category">Commodoenim</span>
								</div>
							</div>
						</a>
					</div>
				</div>
				<div class="col-sm-6 col-md-4 industry factory">
					<div class="box-image-4">
						<a href="project-detail.html" title="Industrial Complex">
							<div class="media">
								<img src="images/project-img-4.jpg" alt="" class="img-responsive">
							</div>
							<div class="body">
								<div class="content">
									<h4 class="title">Iron Industry</h4>
									<span class="category">Commodoenim</span>
								</div>
							</div>
						</a>
					</div>
				</div>
				<div class="col-sm-6 col-md-4 industry oil">
					<div class="box-image-4">
						<a href="project-detail.html" title="Industrial Complex">
							<div class="media">
								<img src="images/project-img-5.jpg" alt="" class="img-responsive">
							</div>
							<div class="body">
								<div class="content">
									<h4 class="title">Gear Manufacturing</h4>
									<span class="category">Commodoenim</span>
								</div>
							</div>
						</a>
					</div>
				</div>
				<div class="col-sm-6 col-md-4 eco">
					<div class="box-image-4">
						<a href="project-detail.html" title="Industrial Complex">
							<div class="media">
								<img src="images/project-img-6.jpg" alt="" class="img-responsive">
							</div>
							<div class="body">
								<div class="content">
									<h4 class="title">Oil Pipeline Industry</h4>
									<span class="category">Commodoenim</span>
								</div>
							</div>
						</a>
					</div>
				</div>
				<div class="col-sm-6 col-md-4 eco gas">
					<div class="box-image-4">
						<a href="project-detail.html" title="Industrial Complex">
							<div class="media">
								<img src="images/project-img-7.jpg" alt="" class="img-responsive">
							</div>
							<div class="body">
								<div class="content">
									<h4 class="title">Oil Pipeline Industry</h4>
									<span class="category">Commodoenim</span>
								</div>
							</div>
						</a>
					</div>
				</div>
				<div class="col-sm-6 col-md-4 eco">
					<div class="box-image-4">
						<a href="project-detail.html" title="Industrial Complex">
							<div class="media">
								<img src="images/project-img-8.jpg" alt="" class="img-responsive">
							</div>
							<div class="body">
								<div class="content">
									<h4 class="title">Oil Pipeline Industry</h4>
									<span class="category">Commodoenim</span>
								</div>
							</div>
						</a>
					</div>
				</div>
				<div class="col-sm-6 col-md-4 oil">
					<div class="box-image-4">
						<a href="project-detail.html" title="Industrial Complex">
							<div class="media">
								<img src="images/project-img-9.jpg" alt="" class="img-responsive">
							</div>
							<div class="body">
								<div class="content">
									<h4 class="title">Oil Pipeline Industry</h4>
									<span class="category">Commodoenim</span>
								</div>
							</div>
						</a>
					</div>
				</div>


			</div>
		</div>
	</div>

	
	<!-- TESTIMONY --> 
	<div class="section testimony bglight">
		<div class="container">

			<div class="row">
				<div class="col-sm-12 col-md-12">
					<h2 class="section-heading">
						Testimonial
					</h2>
				</div>
			</div>

			
			<div class="row">
				
				<div class="col-sm-12 col-md-10 col-md-offset-1">

					
					<div id="owl-testimony2">
						<div class="item">
							<div class="testimonial-3">
					            <div class="media">
					              <img src="images/testimony-thumb-2.jpg" alt="" class="img-circle">
					              <div class="title">Paul Doel</div>
					              <div class="company">alphabeth studio</div>
					            </div>
					            <div class="body">
					              <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry orem Ipsum has been. Mauris ornare tortor in eleifend blanditullam ut ligula et neque. Nulla interdum dapibus erat nec elementum. </p>
					            </div>
				          	</div>
						</div>
						<div class="item">
							<div class="testimonial-3">
					            <div class="media">
					              <img src="images/testimony-thumb-3.jpg" alt="" class="img-circle">
					              <div class="title">Debora Deandra</div>
					              <div class="company">Abc ltd</div>
					            </div>
					            <div class="body">
					              <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry orem Ipsum has been. Mauris ornare tortor in eleifend blanditullam ut ligula et neque. Nulla interdum dapibus erat nec elementum. </p>
					            </div>
				          	</div>
						</div>
						<div class="item">
							<div class="testimonial-3">
					            <div class="media">
					              <img src="images/testimony-thumb-4.jpg" alt="" class="img-circle">
					              <div class="title">Steve Nuer</div>
					              <div class="company">Manufacture ltd</div>
					            </div>
					            <div class="body">
					              <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry orem Ipsum has been. Mauris ornare tortor in eleifend blanditullam ut ligula et neque. Nulla interdum dapibus erat nec elementum. </p>
					            </div>
				          	</div>
						</div>
						<div class="item">
							<div class="testimonial-3">
					            <div class="media">
					              <img src="images/testimony-thumb-5.jpg" alt="" class="img-circle">
					              <div class="title">Robert Lav</div>
					              <div class="company">Gaspol ltd</div>
					            </div>
					            <div class="body">
					              <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry orem Ipsum has been. Mauris ornare tortor in eleifend blanditullam ut ligula et neque. Nulla interdum dapibus erat nec elementum. </p>
					            </div>
				          	</div>
						</div>
						
						
					</div>
					
				</div>

			</div>
		</div>
	</div>
	
	


	<!-- Statistic -->
	<div class="section statistic bgsection">
		<div class="container">
			<div class="row">
				
				<div class="col-sm-3 col-md-3">
					<div class="counter-1">
			            <div class="counter-number">
			              10
			            </div>
			            <div class="counter-title">Offices<br> Worldwide </div>
		          	</div>
				</div>
				<div class="col-sm-3 col-md-3">
					<div class="counter-1">
			            <div class="counter-number">
			              10
			            </div>
			            <div class="counter-title">Refineries &amp; <br>Operations</div>
		          	</div>
				</div>
				<div class="col-sm-3 col-md-3">
					<div class="counter-1">
			            <div class="counter-number">
			              5700
			            </div>
			            <div class="counter-title">Satisfied<br>Employees</div>
		          	</div>
				</div>
				<div class="col-sm-3 col-md-3">
					<div class="counter-1">
			            <div class="counter-number">
			              22
			            </div>
			            <div class="counter-title">Awards &amp; <br>Recognitions</div>
		          	</div>
				</div>

			</div>
		</div>
	</div> 


	<!-- Blog -->
	<div class="section blog">
		<div class="container">
			<div class="row">
				
				<div class="col-sm-12 col-md-12">
					<h2 class="section-heading">
						RECENT NEWS
					</h2>
				</div>

				<div class="col-sm-6 col-md-4">
					<!-- BOX 1 -->
					<div class="box-news-1">
						<div class="media gbr">
							<img src="images/blog-1.jpg" alt="" class="img-responsive">
						</div>
						<div class="body">
							<div class="title"><a href="blog-single.html" title="">The Best in dolor sit amet consectetur adipisicing elit sed</a></div>
							<div class="meta">
								<span class="date"><i class="fa fa-clock-o"></i> Aug 24, 2017</span>
								<span class="comments"><i class="fa fa-comment-o"></i> 0 Comments</span>
							</div>
						</div>
					</div>
				</div>

				<div class="col-sm-6 col-md-4">
					<!-- BOX 2 -->
					<div class="box-news-1">
						<div class="media gbr">
							<img src="images/blog-2.jpg" alt="" class="img-responsive">
						</div>
						<div class="body">
							<div class="title"><a href="blog-single.html" title="">The Best in dolor sit amet consectetur adipisicing elit sed</a></div>
							<div class="meta">
								<span class="date"><i class="fa fa-clock-o"></i> Aug 24, 2017</span>
								<span class="comments"><i class="fa fa-comment-o"></i> 0 Comments</span>
							</div>
						</div>
					</div>
				</div>

				<div class="col-sm-6 col-md-4">
					<!-- BOX 3 -->
					<div class="box-news-1">
						<div class="media gbr">
							<img src="images/blog-3.jpg" alt="" class="img-responsive">
						</div>
						<div class="body">
							<div class="title"><a href="blog-single.html" title="">The Best in dolor sit amet consectetur adipisicing elit sed</a></div>
							<div class="meta">
								<span class="date"><i class="fa fa-clock-o"></i> Aug 24, 2017</span>
								<span class="comments"><i class="fa fa-comment-o"></i> 0 Comments</span>
							</div>
						</div>
					</div>
				</div>

				
			</div>
		</div>
	</div> 

	<!-- CLIENT -->
	<div class="section stat-client">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<div class="col-md-6 " >
						<div class="row box_new_client">
							<div class="row">
								<div class="col-sm-12 col-md-12">
									<h2 class="section-heading">
										Client Logo
									</h2>
								</div>
							</div>
							
							<div class="row">
								<div id="clientlogo1">
										
								
									<div class="item">
										<div class="client-img">
											<a href="#"><img src="images/partners-1.png" alt="" class="img-responsive"></a>
										</div>
									</div>

									<div class="item">
										<div class="client-img">
											<a href="#"><img src="images/partners-2.png" alt="" class="img-responsive"></a>
										</div>
									</div>

									<div class="item">
										<div class="client-img">
											<a href="#"><img src="images/partners-3.png" alt="" class="img-responsive"></a>
										</div>
									</div>

									<div class="item">
										<div class="client-img">
											<a href="#"><img src="images/partners-4.png" alt="" class="img-responsive"></a>
										</div>
									</div>
									<div class="item">
										<div class="client-img">
											<a href="#"><img src="images/partners-2.png" alt="" class="img-responsive"></a>
										</div>
									</div>
									<div class="item">
										<div class="client-img">
											<a href="#"><img src="images/partners-4.png" alt="" class="img-responsive"></a>
										</div>
									</div>
								</div>	

								
							</div>
						</div>
					</div>
					<div class="col-md-6" >
						<div class="row box_new_client">
							<div class="row">
								<div class="col-sm-12 col-md-12">
									<h2 class="section-heading">
										OEM Logo
									</h2>
								</div>
							</div>
							
							<div class="row">
								<div id="clientlogo2">
									<div class="item">
										<div class="client-img">
											<a href="#"><img src="images/partners-1.png" alt="" class="img-responsive"></a>
										</div>
									</div>

									<div class="item">
										<div class="client-img">
											<a href="#"><img src="images/partners-2.png" alt="" class="img-responsive"></a>
										</div>
									</div>

									<div class="item">
										<div class="client-img">
											<a href="#"><img src="images/partners-3.png" alt="" class="img-responsive"></a>
										</div>
									</div>

									<div class="item">
										<div class="client-img">
											<a href="#"><img src="images/partners-4.png" alt="" class="img-responsive"></a>
										</div>
									</div>
									<div class="item">
										<div class="client-img">
											<a href="#"><img src="images/partners-1.png" alt="" class="img-responsive"></a>
										</div>
									</div>

									<div class="item">
										<div class="client-img">
											<a href="#"><img src="images/partners-2.png" alt="" class="img-responsive"></a>
										</div>
									</div>

									<div class="item">
										<div class="client-img">
											<a href="#"><img src="images/partners-3.png" alt="" class="img-responsive"></a>
										</div>
									</div>

									<div class="item">
										<div class="client-img">
											<a href="#"><img src="images/partners-4.png" alt="" class="img-responsive"></a>
										</div>
									</div>
								</div>	

								
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	
	<!-- INFO BOX -->
	<div class="section info overlap-bottom">
		<div class="container">
			<div class="row">
				
				<div class="col-sm-4 col-md-4">
					<!-- BOX 1 -->
					<div class="box-icon-4">
						<div class="icon"><i class="fa fa-phone"></i></div>
						<div class="body-content">
							<div class="heading">CALL US NOW</div>
							Office Telephone: +91 123 123 1234 <br>
							Mobile: +91 123 123 1234
						</div>
					</div>
				</div>
				<div class="col-sm-4 col-md-4">
					<!-- BOX 2 -->
					<div class="box-icon-4">
						<div class="icon"><i class="fa fa-map-marker"></i></div>
						<div class="body-content">
							<div class="heading">COME VISIT US</div>
							New Vip Road Vadodara
						</div>
					</div>
				</div>
				<div class="col-sm-4 col-md-4">
					<!-- BOX 3 -->
					<div class="box-icon-4">
						<div class="icon"><i class="fa fa-envelope"></i></div>
						<div class="body-content">
							<div class="heading">SEND US A MESSAGE</div>
							General: info@Demo.com<br>
							Sales: sales@demo.com
						</div>
					</div>
				</div>
				
			</div>

		</div>
	</div>

	<?php include('include/footer.php'); ?>
	<?php include('include/footerscript.php'); ?>

		
</body>
</html>