
<!DOCTYPE html>
<html lang="zxx">
<?php include('include/headerscript.php'); ?>

<body>

<?php include('include/header.php'); ?>
 
	<!-- BANNER -->
	<div class="section banner-page about">
		<div class="container">
			<div class="row">
				<div class="col-sm-12 col-md-12">
					<div class="title-page">Contact Us</div>
					<ol class="breadcrumb">
						<li><a href="index.php">Home</a></li>
						<li class="active">Contact Us</li>
					</ol>
				</div>
			</div>
		</div>
	</div>

	<!-- Contact -->
	<div class="section contact  ">
		<div class="container">
			<div class="row">
				<div class="col-sm-4 col-md-4 col-md-push-8 " >
					
					<div class="widget contact-info-sidebar">
						<div class="widget-title">
							Contact Info
						</div>
						<ul class="list-info">
							<li>
								<div class="info-icon">
									<span class="fa fa-map-marker"></span>
								</div>
								<div class="info-text">203/Market Point Apartment, Shiyabaug Main Road, Vadodara – 390001 </div> </li>
							<li>
								<div class="info-icon">
									<span class="fa fa-phone"></span>
								</div>
								<div class="info-text">+91 9974620262</div>
							</li>
							<li>
								<div class="info-icon">
									<span class="fa fa-envelope"></span>
								</div>
								<div class="info-text">info@instrumtech.in,</div>
							</li>
							<li>
								<div class="info-icon">
									<span class="fa fa-clock-o"></span>
								</div>
								<div class="info-text">Mon - Sat 09:00 - 17:00</div>
							</li>
						</ul>
					</div> 

				</div>
				<div class="col-sm-8 col-md-8 col-md-pull-4 box_card_product">
					<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3691.732603528903!2d73.13318941442897!3d22.288116748970495!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x395fc62b561ce271%3A0x1d20501549fb058c!2sEverest+Experia!5e0!3m2!1sen!2sin!4v1543831597928" width="750" height="500" frameborder="0" style="border:0" allowfullscreen></iframe>
				</div>

			</div>
			
		</div>
	</div>	


	 
	<?php include('include/footer.php'); ?>
	<?php include('include/footerscript.php'); ?>
		
</body>
</html>