<!-- FOOTER SECTION -->
   <div class="footer">
      
      <div class="container">
         
         <div class="row">
            <div class="col-sm-3 col-md-3">
               <div class="footer-item">
                  <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Necessitatibus esse ipsum odit laudantium molestias eligendi, id laboriosam ipsa, fugiat tenetur qui porro dolores. Vel excepturi beatae inventore, repellendus quo, reiciendis.</p>
                  <div class="footer-sosmed">
                     <a href="#" title="">
                        <div class="item">
                           <i class="fa fa-facebook"></i>
                        </div>
                     </a>
                     <a href="#" title="">
                        <div class="item">
                           <i class="fa fa-twitter"></i>
                        </div>
                     </a>
                     <a href="#" title="">
                        <div class="item">
                           <i class="fa fa-instagram"></i>
                        </div>
                     </a>
                     <a href="#" title="">
                        <div class="item">
                           <i class="fa fa-pinterest"></i>
                        </div>
                     </a> 
                  </div>
               </div>
            </div>
            <div class="col-sm-3 col-md-3">
               <div class="footer-item">
                  <div class="footer-title">
                     Recent Post
                  </div>
                  <ul class="recent-post">
                     <li><a href="#" title="">Vel excepturi beatae inventore, repellendus quo, reiciendis.<</a>
                     <span class="date"><i class="fa fa-clock-o"></i> nov 29, 2018</span></li><li><a href="#" title="">Vel excepturi beatae inventore, repellendus quo, reiciendis.</a>
                     <span class="date"><i class="fa fa-clock-o"></i> nov 29, 2018</span></li>
                  </ul>
               </div>
            </div>
            <div class="col-sm-3 col-md-3">
               <div class="footer-item">
                  <div class="footer-title">
                     Our Services
                  </div>
                  <ul class="list">
                     <li><a href="#" title="">Demo Sevice Title</a></li>
                     <li><a href="#" title="">Demo Sevice Title</a></li>
                     <li><a href="#" title="">Demo Sevice Title</a></li>
                     <li><a href="#" title="">Demo Sevice Title</a></li>
                     <li><a href="#" title="">Demo Sevice Title</a></li>
                     <li><a href="#" title="">Demo Sevice Title</a></li>
                     
                  </ul>
               </div>
            </div>
            <div class="col-sm-3 col-md-3">
               <div class="footer-item">
                  <div class="footer-title">
                     Subscribe
                  </div>
                  <p>Lit sed The Best in dolor sit amet consectetur adipisicing elit sedconsectetur adipisicing</p>
                  <form action="#" class="footer-subscribe">
                       <input type="email" name="EMAIL" class="form-control" placeholder="enter your email">
                       <input id="" type="submit" value="send">
                       <label for="p_submit"><i class="fa fa-envelope"></i></label>
                       <p>Get latest updates and offers.</p>
                     </form>
               </div>
            </div>
         </div>
      </div>
      
      <div class="fcopy">
         <div class="container">
            <div class="row">
               <div class="col-sm-12 col-md-12">
                  <p class="ftex" style="padding-top:10px;" align="center">&copy;2018 softworld Solution</p> 
               </div>
            </div>
         </div>
      </div>
      
   </div>