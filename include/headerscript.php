<head>
    <!-- Basic Page Needs
    ================================================== -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Instrumtech Solution</title>
    <meta name="description" content="#">
    <meta name="keywords" content="#">
    <meta name="author" content="#"> 
      
      <link rel="stylesheet" type="text/css" href="css/vendor/bootstrap.min.css" />
      <link rel="stylesheet" type="text/css" href="css/vendor/font-awesome.min.css">
      <link rel="stylesheet" type="text/css" href="css/vendor/owl.carousel.min.css">
      <link rel="stylesheet" type="text/css" href="css/vendor/owl.theme.default.min.css">
      <!-- <link rel="stylesheet" type="text/css" href="css/vendor/magnific-popup.css"> -->
      
      <!-- ==============================================
      Custom Stylesheet
      =============================================== -->
      <link rel="stylesheet" type="text/css" href="css/style.css" />
      
    <script type="text/javascript" src="js/vendor/modernizr.min.js"></script>

    

</head>