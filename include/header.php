<div class="header">
      <!-- TOPBAR -->
    <div class="topbar">
      <div class="container">
        <div class="row">
          <div class="col-sm-5 col-md-6">
            <div class="topbar-left">
              <div class="welcome-text">
              We help the world growing since 1983
              </div>
            </div>
          </div>
          <div class="col-sm-7 col-md-6">
            <div class="topbar-right">
              <ul class="topbar-menu">
                <li><a href="#" title="Career">Career</a></li>
                <li><a href="#" title="Give Feedback">Give Feedback</a></li>
                <li><a href="#" title="Contact Us">Contact Us</a></li>
              </ul>
              <ul class="topbar-sosmed">
              <li>
                <a href="#"><i class="fa fa-facebook"></i></a>
              </li>
              <li>
                <a href="#"><i class="fa fa-twitter"></i></a>
              </li>
              <li>
                <a href="#"><i class="fa fa-instagram"></i></a>
              </li>
              <li>
                <a href="#"><i class="fa fa-pinterest"></i></a>
              </li>
              </ul>
            </div>
          </div>
        </div>
      </div>
    </div>

    <!-- TOPBAR LOGO SECTION -->
    <div class="topbar-logo">
      <div class="container">
        

        <div class="contact-info">
          <!-- INFO 1 -->
          <div class="box-icon-1">
            <div class="icon">
              <div class="fa fa-envelope-o"></div>
            </div>
            <div class="body-content">
              <div class="heading">Email Support</div>
              info@demo.com
            </div>
          </div>
          <!-- INFO 2 -->
          <div class="box-icon-1">
            <div class="icon">
              <div class="fa fa-phone"></div>
            </div>
            <div class="body-content">
              <div class="heading">Call Support</div>
              +62 123 6700 411
            </div>
          </div>
          <!-- INFO 3 -->
          <a href="inquiry.php" title="" class="btn btn-cta pull-right">GET A QUOTE</a>

        </div>
      </div>
    </div>

    <!-- NAVBAR SECTION -->
    <div class="navbar navbar-main">
    
      <div class="container container-nav">
        <div class="rowe">
            
          <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
            </button>
            
          </div>

          <a class="navbar-brand" href="index.php">
            <img src="images/logo.jpg" style="height: 90px;position: relative;left: -10px;" alt="" />
          </a>

          <nav class="navbar-collapse collapse">
            <ul class="nav navbar-nav navbar-left">
              <li class="dropdown">
                <a href="index.php">HOME</a>
              </li>
               <li><a href="aboutus.php">About Us</a></li>
              <li class="dropdown">
                <a href="products.php" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" role="button" aria-haspopup="true" aria-expanded="false">PRODUCT <span class="caret"></span></a>
                <ul class="dropdown-menu">
                    <li><a href="products.php">Process Instruments</a></li>
                    <li><a href="products.php">Custom Instrumentation Equipment</a></li>
                    <li><a href="products.php">Control Panel</a></li>
                    <li><a href="products.php">Software Development</a></li>
                </ul>
              </li>
              <li><a href="services.php">SERVICES</a></li>
              <li class="dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" role="button" aria-haspopup="true" aria-expanded="false">GALLERY <span class="caret"></span></a>
                <ul class="dropdown-menu">
                    <li><a href="photogallery.php">PHOTO GALLERY</a></li>
                    <li><a href="videogallery.php">VIDEO GALLERY</a></li>
              </ul>
              </li>
              
              
              
              <li><a href="inquiry.php">INQUIRY</a></li>
              <li><a href="contactus.php">CONTACT US</a></li>

            </ul>

            <ul class="nav navbar-nav navbar-right">
              <li class="dropdown">
                  <a href="#" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><i class="fa fa-search"></i></a>
                  <ul class="dropdown-menu">
                  <li>
                    <form class="navbar-form navbar-left" role="search">
                      <div class="form-group">
                        <input type="text" class="form-control" placeholder="Type and hit enter">
                      </div>
                    </form>
                  </li>
                  </ul>
              </li>
            </ul>

          </nav>
            
        </div>
      </div>
      </div>

    </div>