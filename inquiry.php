
<!DOCTYPE html>
<html lang="zxx">
<?php include('include/headerscript.php'); ?>

<body>

<?php include('include/header.php'); ?>
 
	<!-- BANNER -->
	<div class="section banner-page about">
		<div class="container">
			<div class="row">
				<div class="col-sm-12 col-md-12">
					<div class="title-page">Inquiry</div>
					<ol class="breadcrumb">
						<li><a href="index.php">Home</a></li>
						<li class="active">Inquiry</li>
					</ol>
				</div>
			</div>
		</div>
	</div>

	<!-- Contact -->
	<div class="section contact  ">
		<div class="container">
			<div class="row">
				<div class="col-sm-4 col-md-4 col-md-push-8 " >
					
					<img src="images/inquiry.png">

				</div>
				<div class="col-sm-8 col-md-8 col-md-pull-4 box_card_product">
					<div class="content">
						<div class="margin-bottom-30"></div>
						<h3 class="section-heading-2">
							Quick Inquiry
						</h3>
						<form action="#" class="form-contact" novalidate="true">
							<div class="form-group">
								<input type="text" class="form-control" id="p_name" placeholder="Full Name..." required="">
								<div class="help-block with-errors"></div>
							</div>
							<div class="form-group">
								<input type="email" class="form-control" id="p_email" placeholder="Enter Email Address..." required="">
								<div class="help-block with-errors"></div>
							</div>
							<div class="form-group col-md-6" style="padding:0px;">
								<input type="text" class="form-control" id="#" placeholder="Mobile No">
								<div class="help-block with-errors"></div>
							</div>
							<div class="form-group col-md-6" style="padding-left:10px;padding-right:0px;">
								<input type="text" class="form-control" id="p_subject" placeholder="Subject...">
								<div class="help-block with-errors"></div>
							</div>
							<div class="form-group">
								 <textarea id="p_message" class="form-control" rows="6" placeholder="Write message"></textarea>
								<div class="help-block with-errors"></div>
							</div>
							<div class="form-group">
								<div id="success"></div>
								<button type="submit" class="btn btn-secondary disabled" style="pointer-events: all; cursor: pointer;">Submit</button>
							</div>
						</form>
						<div class="margin-bottom-50"></div>
						<p><em>Note: Lorem ipsum dolor sit amet, consectetur adipisicing elit. Magni porro, voluptate, ipsam similique sint eum nisi modi in vel accusamus. Aut assumenda, nisi? Deleniti quidem, obcaecati accusamus sequi ad, enim.</em></p>
					 </div>
				</div>

			</div>
			
		</div>
	</div>	


	 
	<?php include('include/footer.php'); ?>
	<?php include('include/footerscript.php'); ?>
		
</body>
</html>