
<!DOCTYPE html>
<html lang="zxx">
<?php include('include/headerscript.php'); ?>

<body>

<?php include('include/header.php'); ?>
 
	<!-- BANNER -->
	<div class="section banner-page about">
		<div class="container">
			<div class="row">
				<div class="col-sm-12 col-md-12">
					<div class="title-page">Services</div>
					<ol class="breadcrumb">
						<li><a href="index.php">Home</a></li>
						<li class="active">Services</li>
					</ol>
				</div>
			</div>
		</div>
	</div>
	 
	<!-- ABOUT FEATURE -->
	<!-- ABOUT FEATURE -->
	<div class="section why">
		<div class="container " style="padding-bottom:0px;">
			<div class="row box_card_product">
				<div class="col-sm-12 col-md-12">
					<h3>Process Instrument</h3>
					<p>“INSTRUMTECH” is one of the fastest growing supplier & manufacturer of the process instruments. Our high-end products are perfect combination of excellent visual aesthetics, quality assurance and durable technology.</p> 
				</div>
			</div>
		</div>
	</div>

	<div class="section service section-border bglight">
		<div class="container" style="padding-top:0px;">
			<div class="row">
				<div class="col-sm-12 col-md-12">
					<h2 class="section-heading">
						SERVICES
					</h2>
				</div>
			</div>

			<div class="row">

				<div class="col-sm-6 col-md-4">
					<!-- BOX 1 -->
					<div class="feature-box-8">
		              <div class="media">
		                <img src="images/service-1.jpg" alt="rud" class="img-responsive">
		              </div>
		              <div class="body">
		                <div class="icon-holder">
		                  <span class="fa fa-gears"></span>
		                </div>
		                <a href="services-detail.html" class="title">MECHANICAL ENGINEERING</a>
		                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit sed</p>
		                <a href="services-detail.html" class="readmore">READ MORE</a>
		              </div>
		            </div>
				</div>
				<div class="col-sm-6 col-md-4">
					<!-- BOX 2 -->
					<div class="feature-box-8">
		              <div class="media">
		                <img src="images/service-2.jpg" alt="rud" class="img-responsive">
		              </div>
		              <div class="body">
		                <div class="icon-holder">
		                  <span class="fa fa-leaf"></span>
		                </div>
		                <a href="services-detail.html" class="title">AGRICULTURAL PROCESSING</a>
		                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit sed</p>
		                <a href="services-detail.html" class="readmore">READ MORE</a>
		              </div>
		            </div>
				</div>
				<div class="col-sm-6 col-md-4">
					<!-- BOX 3 -->
					<div class="feature-box-8">
		              <div class="media">
		                <img src="images/service-3.jpg" alt="rud" class="img-responsive">
		              </div>
		              <div class="body">
		                <div class="icon-holder">
		                  <span class="fa fa-fire"></span>
		                </div>
		                <a href="services-detail.html" class="title">OILS AND LUBRICANTS</a>
		                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit sed</p>
		                <a href="services-detail.html" class="readmore">READ MORE</a>
		              </div>
		            </div>
				</div>
				<div class="col-sm-6 col-md-4">
					<!-- BOX 4 -->
					<div class="feature-box-8">
		              <div class="media">
		                <img src="images/service-4.jpg" alt="rud" class="img-responsive">
		              </div>
		              <div class="body">
		                <div class="icon-holder">
		                  <span class="fa fa-flash"></span>
		                </div>
		                <a href="services-detail.html" class="title">POWER AND ENERGY</a>
		                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit sed</p>
		                <a href="services-detail.html" class="readmore">READ MORE</a>
		              </div>
		            </div>
	          	</div>
				<div class="col-sm-6 col-md-4">
					<!-- BOX 5 -->
					<div class="feature-box-8">
		              <div class="media">
		                <img src="images/service-5.jpg" alt="rud" class="img-responsive">
		              </div>
		              <div class="body">
		                <div class="icon-holder">
		                  <span class="fa fa-flask"></span>
		                </div>
		                <a href="services-detail.html" class="title">CHEMICAL RESEARCH</a>
		                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit sed</p>
		                <a href="services-detail.html" class="readmore">READ MORE</a>
		              </div>
		            </div>
				</div>
				<div class="col-sm-6 col-md-4">
					<!-- BOX 6 -->
					<div class="feature-box-8">
		              <div class="media">
		                <img src="images/service-6.jpg" alt="rud" class="img-responsive">
		              </div>
		              <div class="body">
		                <div class="icon-holder">
		                  <span class="fa fa-cubes"></span>
		                </div>
		                <a href="services-detail.html" class="title">MATERIAL ENGINEERING</a>
		                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit sed</p>
		                <a href="services-detail.html" class="readmore">READ MORE</a>
		              </div>
		            </div>
				</div>			
				
			</div>
		</div>
	</div>
	

	
	
	 
	<?php include('include/footer.php'); ?>
	<?php include('include/footerscript.php'); ?>
		
</body>
</html>